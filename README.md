What are these files?
====
DNN_Pracw_20.pickle: The trained model, to predict the value of PRACW in 20um.

DNN_Pracw_32.pickle: The trained model, to predict the value of PRACW in 32um.

DNN_Pracw_40.pickle: The trained model, to predict the value of PRACW in 40um.

DNN_Pracw_50.pickle: The trained model, to predict the value of PRACW in 50um.

DNN_Praut_20.pickle: The trained model, to predict the value of PRAUT in 20um.

DNN_Praut_32.pickle: The trained model, to predict the value of PRAUT in 32um.

DNN_Praut_40.pickle: The trained model, to predict the value of PRAUT in 40um.

DNN_Praut_50.pickle: The trained model, to predict the value of PRAUT in 50um.

DNN.py: You need to run it to get the predicted value.

How to run
====
1. Install python moudle. (Pandas, pickle, numpy, sklearn, pytorch)
   In terminal run
       
       sudo pip install pandas

       sudo pip intsall pickle

       sudo pip install numpy

       sudo pip install sklearn

       sudo pip install pip
       
  to get needed moudle for python code.

2. Choose the dataset you want to use to predict 'PRACW' or 'PRAUT'.
   (DNN.py: In line 12. )
        
        dataset = pd.read_csv('weather_pracw_20um_all.csv')

   Change the file name 'weather_pracw_20um_all.csv' to the dataset you want to use to predict PRACW or PRAUT. Please ensure your datadet is a .csv file,  and data in columns 1 to 5 are in the following order: cloud mass, cloud number, rain mass, rain number, PRACW or PRAUT. The value of PRACW should be greater than 0 (PRACW > 0).

3. Choose the trained model that you need.
   (DNN.py: In line 39. )

       with open('DNN_Pracw_20.pickle', 'rb') as file: 


4. Run the code.

5. For more details please refer to my master thesis in ./paper/Canran Li-Master dissertation.pdf.
