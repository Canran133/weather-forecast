import pandas as pd
import pickle
import numpy as np
from sklearn import preprocessing
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
import torch
import torch.nn.functional as F

#Load data from csv file
dataset = pd.read_csv('weather_pracw_20um.csv')#Change the file name to the dataset you want to use to predict the pracw
# dataset = dataset[dataset['pracw_20um']>0]
input_data = dataset.ix[:,:4].to_numpy()
real_pracw = dataset.ix[:,4:5].to_numpy()

#Data pre-processing
scaler_x = preprocessing.StandardScaler()
scaler_x.fit(input_data)
scaler_y = preprocessing.StandardScaler()#use this to inverse the predict value
scaler_y.fit(real_pracw)
input_data = scaler_x.transform(input_data)# turning the input data to range(0~1)
input_data = torch.from_numpy(input_data).float()#turning the input data to torch type
class Net(torch.nn.Module):
        def __init__(self, n_feature, n_hidden, n_output):
            super(Net, self).__init__()
            self.hidden = torch.nn.Linear(n_feature, n_hidden)  
            self.hidden2 = torch.nn.Linear(n_hidden, n_hidden)            
            self.out = torch.nn.Linear(n_hidden, n_output)  

        def forward(self, x):
            x = F.relu(self.hidden(x))  
            for i in range(1, 3):
                x = F.relu(self.hidden2(x))
            x = self.out(x)
            return x

#Load model
with open('DNN.pickle', 'rb') as file:    
    model = pickle.load(file)
    #Predict Pracw
    predict_pracw = model(input_data).detach().numpy()
    predict_pracw = scaler_y.inverse_transform(predict_pracw)
    predict_pracw = np.array(predict_pracw)
    print(predict_pracw)
    #Evaluation metrics
    mse = mean_squared_error(real_pracw, predict_pracw)
    mae = mean_absolute_error(real_pracw, predict_pracw)
    r2 = r2_score(real_pracw, predict_pracw)
    print("MSE: ", mse)
    print("MAE: ", mae)
    print("R2: ", r2)
    #np.savetxt('DNN_predict_value_20um.csv', predict_pracw)#Uncomment this line to save the predict value to a .csv file
